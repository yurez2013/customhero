<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/hero/{hero}', 'HeroController@index');
Route::get('/contact_us', 'ContactController@index')->name('contact_us');
Route::post('/contact_form', 'ContactController@contact_form')->name('contact_form');

Auth::routes();

Route::get('/admin', 'HeroSkillsController@index')->name('admin');
Route::get('/hero-skills-create', 'HeroSkillsController@create')->name('hero-skills-create');
Route::post('/hero-skills-save', 'HeroSkillsController@save')->name('hero-skills-save');
