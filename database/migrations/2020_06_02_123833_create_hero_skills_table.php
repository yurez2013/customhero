<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeroSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hero_skills', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('nikname');
            $table->string('mmr');
            $table->string('hero');
            $table->string('skills_1');
            $table->string('skills_2');
            $table->string('skills_3');
            $table->string('skills_4');
            $table->string('skills_5');
            $table->string('skills_6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hero_skills');
    }
}
