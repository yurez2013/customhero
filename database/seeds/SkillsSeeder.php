<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            'MistCoil',
            'AphoticShield',
            'CurseofAvernus',
            'BorrowedTime',
            'AcidSpray',
            'UnstableConcoction',
            'GreevilsGreed',
            'ChemicalRage',
            'ColdFeet',
            'IceVortex',
            'ChillingTouch',
            'IceBlast',
            'ManaBreak',
            'Blink',
            'Counterspell',
            'ManaVoid',
            'Flux',
            'MagneticField',
            'SparkWraith',
            'TempestDouble',
            'BerserkerCall',
            'BattleHunger',
            'CounterHelix',
            'CullingBlade',
            'Nightmare',
            'BrainSap',
            'Enfeeble',
            'FiendsGrip',
            'StickyNapalm',
            'Flamebreak',
            'Firefly',
            'FlamingLasso',
            'WildAxes',
            'CalloftheWildBoar',
            'CalloftheWildHawk',
            'InnerBeast',
            'PrimalRoar',
            'Bloodrage',
            'BloodRite',
            'Thirst',
            'Rupture',
            'ShurikenToss',
            'Jinada',
            'ShadowWalk',
            'Track',
            'ThunderClap',
            'CinderBrew',
            'DrunkenBrawler',
            'PrimalSplit',
            'ViscousNasalGoo',
            'QuillSpray',
            'Bristleback',
            'Warpath',
            'SpawnSpiderlings',
            'SpinWeb',
            'IncapacitatingBite',
            'InsatiableHunger',
            'HoofStomp',
            'DoubleEdge',
            'Retaliate',
            'Stampede',
            'ChaosBolt',
            'RealityRift',
            'ChaosStrike',
            'Phantasm',
            'Penitence',
            'HolyPersuasion',
            'DivineFavor',
            'HandofGod',
            'DeathPact',
            'SearingArrows',
            'SkeletonWalk',
            'BurningArmy',
            'CrystalNova',
            'Frostbite',
            'ArcaneAura',
            'FreezingField',
            'Vacuum',
            'IonShell',
            'Surge',
            'WallofReplica',
            'BrambleMaze',
            'ShadowRealm',
            'CursedCrown',
            'Bedlam',
            'Terrorize',
            'PoisonTouch',
            'ShallowGrave',
            'ShadowWave',
            'BadJuju',
            'CryptSwarm',
            'Silence',
            'SpiritSiphon',
            'Exorcism',
            'ThunderStrike',
            'Glimpse',
            'KineticField',
            'StaticStorm',
            'Devour',
            'ScorchedEarth',
            'InfernalBlade',
            'Doom',
            'BreatheFire',
            'DragonTail',
            'DragonBlood',
            'ElderDragonForm',
            'FrostArrows',
            'Gust',
            'Multishot',
            'Marksmanship',
            'BoulderSmash',
            'RollingBoulder',
            'GeomagneticGrip',
            'Magnetize',
            'Fissure',
            'EnchantTotem',
            'Aftershock',
            'EchoSlam',
            'EchoStomp',
            'AstralSpirit',
            'NaturalOrder',
            'EarthSplitter',
            'SearingChains',
            'SleightofFist',
            'FlameGuard',
            'FireRemnant',
            'Impetus',
            'Enchant',
            'NaturesAttendants',
            'Untouchable',
            'Malefice',
            'DemonicConversion',
            'MidnightPulse',
            'BlackHole',
            'TimeWalk',
            'TimeDilation',
            'TimeLock',
            'Chronosphere',
            'StrokeofFate',
            'PhantomsEmbrace',
            'InkSwell',
            'Soulbind',
            'RocketBarrage',
            'HomingMissile',
            'FlakCannon',
            'CallDown',
            'InnerFire',
            'BurningSpear',
            'BerserkersBlood',
            'LifeBreak',
            'Quas',
            'Wex',
            'Exort',
            'Invoke',
            'Tether',
            'Spirits',
            'Overcharge',
            'Relocate',
            'DualBreath',
            'IcePath',
            'LiquidFire',
            'Macropyre',
            'BladeFury',
            'HealingWard',
            'BladeDance',
            'Swiftslash',
            'Omnislash',
            'Illuminate',
            'BlindingLight',
            'ChakraMagic',
            'Will-O-Wisp',
            'Torrent',
            'Tidebringer',
            'XMarkstheSpot',
            'Ghostship',
            'OverwhelmingOdds',
            'PressTheAttack',
            'MomentofCourage',
            'Duel',
            'SplitEarth',
            'DiabolicEdict',
            'LightningStorm',
            'PulseNova',
            'FrostBlast',
            'FrostShield',
            'SinisterGaze',
            'ChainFrost',
            'Rage',
            'Feast',
            'OpenWounds',
            'Infest',
            'DragonSlave',
            'LightStrikeArray',
            'FierySoul',
            'LagunaBlade',
            'EarthSpike',
            'Hex',
            'ManaDrain',
            'FingerofDeath',
            'SummonSpiritBear',
            'SpiritLink',
            'SavageRoar',
            'TrueForm',
            'LucentBeam',
            'MoonGlaives',
            'LunarBlessing',
            'Eclipse',
            'SummonWolves',
            'Howl',
            'FeralImpulse',
            'Shapeshift',
            'Shockwave',
            'Empower',
            'Skewer',
            'ReversePolarity',
            'SpearofMars',
            'GodsRebuke',
            'Bulwark',
            'ArenaOfBlood',
            'SplitShot',
            'MysticSnake',
            'ManaShield',
            'StoneGaze',
            'Earthbind',
            'Poof',
            'Ransack',
            'DividedWeStand',
            'Starstorm',
            'SacredArrow',
            'Leap',
            'MoonlightShadow',
            'BoundlessStrike',
            'TreeDance',
            'JinguMastery',
            'WukongsCommand',
            'Waveform',
            'AdaptiveStrikeAgility',
            'AdaptiveStrikeStrength',
            'AttributeShiftAgilityGain',
            'Morph',
            'MirrorImage',
            'Ensnare',
            'RipTide',
            'SongoftheSiren',
            'Sprout',
            'Teleportation',
            'NaturesCall',
            'WrathofNature',
            'DeathPulse',
            'GhostShroud',
            'HeartstopperAura',
            'ReapersScythe',
            'Void',
            'CripplingFear',
            'HunterintheNight',
            'DarkAscension',
            'Impale',
            'ManaBurn',
            'SpikedCarapace',
            'Vendetta',
            'FortunesEnd',
            'FatesEdict',
            'PurifyingFlames',
            'FalsePromise',
            'ArcaneOrb',
            'AstralImprisonment',
            'EssenceFlux',
            'SanitysEclipse',
            'Swashbuckle',
            'ShieldCrash',
            'LuckyShot',
            'RollingThunder',
            'StiflingDagger',
            'PhantomStrike',
            'Blur',
            'CoupdeGrace',
            'SpiritLance',
            'Doppelganger',
            'PhantomRush',
            'Juxtapose',
            'IcarusDive',
            'FireSpirits',
            'SunRay',
            'Supernova',
            'IllusoryOrb',
            'WaningRift',
            'PhaseShift',
            'DreamCoil',
            'MeatHook',
            'Rot',
            'FleshHeap',
            'Dismember',
            'NetherBlast',
            'Decrepify',
            'NetherWard',
            'LifeDrain',
            'ShadowStrike',
            'Blink',
            'ScreamOfPain',
            'SonicWave',
            'PlasmaField',
            'StaticLink',
            'StormSurge',
            'EyeoftheStorm',
            'SmokeScreen',
            'BlinkStrike',
            'TricksoftheTrade',
            'CloakandDagger',
            'Telekinesis',
            'FadeBolt',
            'ArcaneSupremacy',
            'SpellSteal',
            'Burrowstrike',
            'SandStorm',
            'CausticFinale',
            'Epicenter',
            'Disruption',
            'SoulCatcher',
            'ShadowPoison',
            'DemonicPurge',
            'Shadowraze',
            'Necromastery',
            'PresenceoftheDarkLord',
            'RequiemofSouls',
            'EtherShock',
            'Hex',
            'Shackles',
            'MassSerpentWard',
            'ArcaneCurse',
            'GlaivesofWisdom',
            'LastWord',
            'GlobalSilence',
            'ArcaneBolt',
            'ConcussiveShot',
            'AncientSeal',
            'MysticFlare',
            'GuardianSprint',
            'SlithereenCrush',
            'BashoftheDeep',
            'CorrosiveHaze',
            'DarkPact',
            'Pounce',
            'EssenceShift',
            'ShadowDance',
            'Scatterblast',
            'FiresnapCookie',
            'LilShredder',
            'SpitOut',
            'MortimerKisses',
            'Shrapnel',
            'Headshot',
            'TakeAim',
            'Assassinate',
            'SpectralDagger',
            'Desolate',
            'Dispersion',
            'Haunt',
            'ChargeofDarkness',
            'Bulldoze',
            'GreaterBash',
            'NetherStrike',
            'StaticRemnant',
            'ElectricVortex',
            'Overload',
            'BallLightning',
            'StormHammer',
            'GreatCleave',
            'Warcry',
            'GodsStrength',
            'ProximityMines',
            'StasisTrap',
            'BlastOff',
            'MinefieldSign',
            'RemoteMines',
            'Refraction',
            'Meld',
            'PsiBlades',
            'PsionicTrap',
            'Reflection',
            'ConjureImage',
            'Metamorphosis',
            'TerrorWave',
            'Sunder',
            'Gush',
            'KrakenShell',
            'AnchorSmash',
            'Ravage',
            'WhirlingDeath',
            'TimberChain',
            'ReactiveArmor',
            'Chakram',
            'Laser',
            'HeatSeekingMissile',
            'MarchoftheMachines',
            'Rearm',
            'Avalanche',
            'Toss',
            'TreeGrab',
            'Grow',
            'NaturesGrasp',
            'LeechSeed',
            'LivingArmor',
            'Overgrowth',
            'BerserkersRage',
            'WhirlingAxesRanged',
            'Fervor',
            'BattleTrance',
            'IceShards',
            'Snowball',
            'TagTeam',
            'WalrusPUNCH',
            'Firestorm',
            'PitofMalice',
            'AtrophyAura',
            'DarkRift',
            'Decay',
            'SoulRip',
            'Tombstone',
            'FleshGolem',
            'Earthshock',
            'Overpower',
            'FurySwipes',
            'Enrage',
            'MagicMissile',
            'WaveofTerror',
            'VengeanceAura',
            'NetherSwap',
            'VenomousGale',
            'PoisonSting',
            'PlagueWard',
            'PoisonNova',
            'PoisonAttack',
            'Nethertoxin',
            'CorrosiveSkin',
            'ViperStrike',
            'GraveChill',
            'SoulAssumption',
            'GravekeepersCloak',
            'SummonFamiliars',
            'AetherRemnant',
            'Dissimilate',
            'ResonantPulse',
            'AstralStep',
            'FatalBonds',
            'ShadowWord',
            'Upheaval',
            'ChaoticOffering',
            'TheSwarm',
            'Shukuchi',
            'GeminateAttack',
            'TimeLapse',
            'Shackleshot',
            'Powershot',
            'Windrun',
            'FocusFire',
            'ArcticBurn',
            'SplinterBlast',
            'ColdEmbrace',
            'WintersCurse',
            'ParalyzingCask',
            'VoodooRestoration',
            'Maledict',
            'DeathWard',
            'WraithfireBlast',
            'VampiricAura',
            'MortalStrike',
            'Reincarnation',
            'ArcLightning',
            'LightningBolt',
            'StaticField',
            'ThundergodsWrath',
        ];

        $collection = collect($skills);
        $collection->each(function ($item, $key) {
            DB::table('skills')->insert([
                'name' => $item,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        });
    }
}
