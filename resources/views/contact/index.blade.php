@extends('layouts.layout')

@section('content')
<!-- Contact -->
<section class="site-content site-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 site-block">
                <h3 class="h2 site-heading"><strong>Contact</strong> Form</h3>
                <form action="{{ route('contact_form') }}" method="POST" id="form-contact">
                    @method('POST')
                    @csrf
                    <div class="form-group">
                        <label for="contact-name">Name</label>
                        <input type="text" id="contact-name" value="{{ old('name') }}" name="name" class="form-control input-lg" placeholder="Your name..">
                    </div>
                    <div class="form-group">
                        <label for="contact-email">Email</label>
                        <input type="text" id="contact-email" value="{{ old('email') }}" name="email" class="form-control input-lg" placeholder="Your email..">
                    </div>
                    <div class="form-group">
                        <label for="contact-message">Message</label>
                        <textarea id="contact-message" name="message" rows="10" class="form-control input-lg" placeholder="Let us know how we can assist..">
                            {{{ old('message') }}}
                        </textarea>
                    </div>
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-primary">Send Message</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>
<!-- END Contact -->
@endsection
