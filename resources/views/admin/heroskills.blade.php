@extends('layouts.admin')

@section('content')

    <div id="main-container">

        <!-- Page content -->
        <div id="page-content">

            <!-- Quick Stats -->
            <div class="row text-center">
                <div class="col-sm-6 col-lg-3">
                    <a href="{{ route('hero-skills-create') }}" class="widget widget-hover-effect2">
                        <div class="widget-extra themed-background">
                            <h4 class="widget-content-light"><strong>Add</strong></h4>
                        </div>
                    </a>
                </div>
            </div>
            <!-- END Quick Stats -->

            <!-- All Orders Block -->
            <div class="block full">
                <!-- All Orders Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                    </div>
                    <h2><strong>All</strong> Orders</h2>
                </div>
                <!-- END All Orders Title -->

                <!-- All Orders Content -->
                <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 100px;">ID</th>
                        <th class="text-center visible-lg">Hero</th>
                        <th class="text-center visible-lg">Skills 1</th>
                        <th class="text-center visible-lg">Skills 2</th>
                        <th class="text-center visible-lg">Skills 3</th>
                        <th class="text-center visible-lg">Skills 4</th>
                        <th class="text-center visible-lg">Skills 5</th>
                        <th class="text-center visible-lg">Skills 6</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($heros_skills as $heros_skill)
                    <tr>
                        <td class="text-center">
                            {{$heros_skill->id}}
                        </td>
                        <td class="text-center">
                            <img style="width: 50%;" src="{{ url('/assets/img/hero/'.$heros_skill->heros->name.'.jpg') }}" alt="image" class="img-responsive">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills1->name.'.jpg') }}" alt="">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills2->name.'.jpg') }}" alt="">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills3->name.'.jpg') }}" alt="">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills4->name.'.jpg') }}" alt="">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills5->name.'.jpg') }}" alt="">
                        </td>
                        <td class="text-center">
                            <img  style="width: 30%;"  class="img-responsive" src="{{ url('/assets/img/skills/'.$heros_skill->skills6->name.'.jpg') }}" alt="">
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <!-- END All Orders Content -->
            </div>
            <!-- END All Orders Block -->
        </div>
        <!-- END Page Content -->

        <!-- Footer -->
        <footer class="clearfix">
            <div class="pull-right">
                Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
            </div>
            <div class="pull-left">
                <span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.8</a>
            </div>
        </footer>
        <!-- END Footer -->
    </div>
    <!-- END Main Container -->
@endsection
