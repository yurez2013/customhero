@extends('layouts.admin')

@section('content')

<!-- Main Container -->
<div id="main-container">

    <!-- Page content -->
    <div id="page-content">

        <!-- Product Edit Content -->
        <div class="row">
            <div class="col-lg-12">
                <!-- General Data Block -->
                <div class="block">
                    <!-- General Data Title -->
                    <div class="block-title">
                        <h2><i class="fa fa-pencil"></i> <strong>Add</strong> hero skills</h2>
                    </div>
                    <!-- END General Data Title -->

                    <!-- General Data Content -->
                    <form action="{{ route('hero-skills-save') }}" method="POST" class="form-horizontal form-bordered">
                        @method('POST')
                    @csrf
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-id">Title</label>
                            <div class="col-md-9">
                                <input type="text" id="product-id" name="title" class="form-control" placeholder="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-id">Nikname</label>
                            <div class="col-md-9">
                                <input type="text" id="product-id" name="nikname" class="form-control" placeholder="nikname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-id">MMR</label>
                            <div class="col-md-9">
                                <input type="text" id="product-id" name="mmr" class="form-control" placeholder="mmr">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Hero</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="hero" class="select-chosen" data-placeholder="Choose Hero.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($heros as $key => $hero)
                                        <option value="{{ $key }}">{{$hero->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 1</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills1" class="select-chosen" data-placeholder="Choose Skills 1.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 2</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills2" class="select-chosen" data-placeholder="Choose Skills 2.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 3</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills3" class="select-chosen" data-placeholder="Choose Skills 3.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 4</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills4" class="select-chosen" data-placeholder="Choose Skills 4.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 5</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills5" class="select-chosen" data-placeholder="Choose Skills 5.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="product-category">Skills 6</label>
                            <div class="col-md-8">
                                <!-- Chosen plugin (class is initialized in js/app.js -> uiInit()), for extra usage examples you can check out http://harvesthq.github.io/chosen/ -->
                                <select id="product-category" name="skills6" class="select-chosen" data-placeholder="Choose Skills 6.." style="width: 250px;">
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                    @foreach($skills as $key => $skill)
                                        <option value="{{ $key }}">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                    </form>
                    <!-- END General Data Content -->
                </div>
                <!-- END General Data Block -->
            </div>

        </div>
        <!-- END Product Edit Content -->
    </div>
    <!-- END Page Content -->

    <!-- Footer -->
    <footer class="clearfix">
        <div class="pull-right">
            Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
        </div>
        <div class="pull-left">
            <span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.8</a>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Main Container -->
@endsection
