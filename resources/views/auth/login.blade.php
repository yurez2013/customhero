@extends('layouts.layout')

@section('content')
    <!-- Log In -->
    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 site-block">
                    <!-- Log In Form -->
                    <form action="{{ route('login') }}" method="POST" id="form-log-in" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                    <input type="email" id="login-email" name="email" class="form-control input-lg @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                    <input type="password" id="login-password" name="password" class="form-control input-lg @error('password') is-invalid @enderror" placeholder="Password" autocomplete="current-password">

                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-xs-6">
                                <label class="switch switch-primary">
                                    <input type="checkbox" id="login-remember-me" name="login-remember-me" checked><span></span>
                                </label>
                                <small>Remember me</small>
                            </div>
                            <div class="col-xs-6 text-right">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Log In</button>
                            </div>
                        </div>
                        <div class="form-group">

                        </div>
                    </form>
                    <div class="text-center">

                        <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                    </div>
                    <!-- END Log In Form -->
                </div>
            </div>
            <hr>
        </div>
    </section>
    <!-- END Log In -->
@endsection

