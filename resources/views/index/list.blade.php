@extends('layouts.layout')

@section('content')
    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                <!-- Posts -->
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <!-- Blog Post -->
                    @foreach($heros_skills as $hero)
                    <div class="site-block" style="background: #f1f3f9;padding: 1em;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <p>
                                    <img style="width: 100%;" src="{{ url('/assets/img/hero/'.$hero->heros->name.'.jpg') }}" alt="image" class="img-responsive">
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8">
                                <h3 class="site-heading"><strong>{{ $hero->title }}</strong></h3>
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills1->name.'.jpg') }}" alt="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills2->name.'.jpg') }}" alt="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills3->name.'.jpg') }}" alt="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills4->name.'.jpg') }}" alt="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills5->name.'.jpg') }}" alt="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <img class="img-responsive" src="{{ url('/assets/img/skills/'.$hero->skills6->name.'.jpg') }}" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;">
                                <ul class="list-inline pull-left">
                                    <li><i class="fa fa-bar-chart"></i> <span style="color: #394263; font-weight: bold;">{{ $hero->mmr }}</span> MMR</li>
                                    <li><i class="fa fa-calendar"></i> {{ $hero->created_at }}</li>
                                    <li><i class="fa fa-user"></i> by <a href="javascript:void(0)">{{ $hero->nikname }}</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <!-- END Blog Post -->
                    @endforeach
{{--                    <!-- Pagination -->--}}
{{--                    <div class="text-right">--}}
{{--                        <ul class="pagination">--}}
{{--                            <li><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>--}}
{{--                            <li class="active"><a href="javascript:void(0)">1</a></li>--}}
{{--                            <li><a href="javascript:void(0)">2</a></li>--}}
{{--                            <li><a href="javascript:void(0)">3</a></li>--}}
{{--                            <li><a href="javascript:void(0)">4</a></li>--}}
{{--                            <li><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <!-- END Pagination -->--}}
                </div>
                <!-- END Posts -->

                <!-- Sidebar -->
                <div class="col-xs-12 col-sm-12 col-md-3">
                    <aside class="sidebar site-block">
                        <div class="sidebar-block">
                            <h3 class="site-heading">List Hero</h3>
                            <ul class="store-menu">
                                @foreach($heros as $key => $value)
                                <li class="{{(isset($slug[0]) && $slug[0] === $key) ? 'open' : (!isset($slug[0]) && $key === 'a') ? 'open' : ''}}">
                                        <a href="javascript:void(0)" class="submenu"><i class="fa fa-angle-right"></i> {{$key}}</a>
                                        <ul>
                                            @foreach($heros[$key] as $hero)
                                                <li><a style="{{isset($slug[0]) && $slug === $hero ? 'font-weight: bold;' : ''}}"  href="{{ url('/hero/'.$hero) }}">{{$hero}}</a></li>

                                            @endforeach
                                        </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>


                    </aside>
                </div>
                <!-- END Sidebar -->
            </div>
        </div>
    </section>
@endsection
