<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hero extends Model
{
    protected $table = 'hero';

    public function heroskills()
    {
        return $this->belongsToMany('App\HeroSkills', 'id', 'hero');
    }
}
