<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HeroSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'nikname' => 'required|max:255',
            'mmr' => 'required|numeric',
            'hero' => 'required|numeric',
            'skills1' => 'required|numeric',
            'skills2' => 'required|numeric',
            'skills3' => 'required|numeric',
            'skills4' => 'required|numeric',
            'skills5' => 'required|numeric',
            'skills6' => '',
        ];
    }
}

