<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use Carbon\Carbon;


class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function contact_form(ContactRequest $request)
    {

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $contact->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $contact->save();

        return redirect()->back()->with('message', 'Message send!');
    }

}
