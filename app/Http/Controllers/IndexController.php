<?php

namespace App\Http\Controllers;

use App\Hero;
use App\HeroSkills;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function index()
    {
        $heros = Hero::all();
        $grouped = $heros->mapToGroups(function ($item, $key) {
            return [$item->name[0] => $item->name];
        });

        return view('index.list', [
            'heros' => $grouped,
            'heros_skills' => HeroSkills::orderByDesc('id')->get()
        ]);
    }

}
