<?php

namespace App\Http\Controllers;

use App\Hero;
use App\HeroSkills;
use Illuminate\Http\Request;

class HeroController extends Controller
{

    public function index(Request $request, $hero)
    {
        $hero  = Hero::where('name', $hero)->first();
        $heros = Hero::all();
        $grouped = $heros->mapToGroups(function ($item, $key) {
            return [$item->name[0] => $item->name];
        });

        return view('index.list', [
            'heros' => $grouped,
            'heros_skills' => HeroSkills::where('hero', $hero->id)->orderByDesc('id')->get(),
            'slug' => $hero->name
        ]);
    }

}
