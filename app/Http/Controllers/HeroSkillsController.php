<?php

namespace App\Http\Controllers;

use App\Hero;
use App\HeroSkills;
use App\Http\Requests\HeroSkillRequest;
use App\Skills;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HeroSkillsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.heroskills', [
            'heros_skills' => HeroSkills::all()
        ]);
    }

    public function create()
    {
        return view('admin.heroskills_create', [
            'heros' => Hero::all(),
            'skills' => Skills::all(),
        ]);
    }

    public function save(HeroSkillRequest $request)
    {
        $hero_skills = new HeroSkills();
        $hero_skills->title = $request->title;
        $hero_skills->nikname = $request->nikname;
        $hero_skills->mmr = $request->mmr;
        $hero_skills->hero = $request->hero + 1;
        $hero_skills->skills_1 = $request->skills1 + 1;
        $hero_skills->skills_2 = $request->skills2 + 1;
        $hero_skills->skills_3 = $request->skills3 + 1;
        $hero_skills->skills_4 = $request->skills4 + 1;
        $hero_skills->skills_5 = $request->skills5 + 1;
        $hero_skills->skills_6 = $request->skills6 + 1;
        $hero_skills->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $hero_skills->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $hero_skills->save();

        return redirect('admin');
    }


}
