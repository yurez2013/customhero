<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeroSkills extends Model
{
    protected $table = 'hero_skills';

    public function skills1()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_1');
    }

    public function skills2()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_2');
    }

    public function skills3()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_3');
    }

    public function skills4()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_4');
    }

    public function skills5()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_5');
    }

    public function skills6()
    {
        return $this->hasOne('App\Skills', 'id', 'skills_6');
    }

    public function heros()
    {
        return $this->hasOne('App\Hero', 'id', 'hero');
    }
}
