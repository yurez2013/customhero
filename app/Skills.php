<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model
{
    protected $table = 'skills';

    public function heroskills1()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_1');
    }

    public function heroskills2()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_2');
    }

    public function heroskills3()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_3');
    }

    public function heroskills4()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_4');
    }

    public function heroskills5()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_5');
    }

    public function heroskills6()
    {
        return $this->belongsTo('App\HeroSkills', 'id', 'skills_6');
    }
}
